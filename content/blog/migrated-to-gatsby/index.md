---
title: I migrated to gatsby
date: "2021-03-02T22:32:00.000Z"
description: "I migrated this blog to gatsby"
---

Hi everyone,

It has been a while since I have not post anything here.
As I want to take some time again to post some things here, I was searching for a simpler and lighter solution that my wordpress blog.
So I am trying gatsby which is a framework allowing me to write my posts in markdown and generating static website.

I just need to push built website into a docker container comtaining a web server and all is working great and smoothly with no need for a database or an application server like php.
